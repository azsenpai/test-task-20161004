jQuery(function($) {
    var $coord_x_value = $('.coord-x .value');
    var $coord_y_value = $('.coord-y .value');

    var $number_value = $('.number .value');

    var focused = false;

    $('.form-input-a').on({
        mouseover: function() {
            if (!focused) {
                show_info($(this));
            }
        },
        mouseout: function() {
            if (!focused) {
                hide_info();
            }
        },
        focusin: function() {
            focused = true;
            show_info($(this));
        },
        focusout: function() {
            focused = false;
            hide_info();
        },
        keypress: function(e) {
            return e.keyCode != 13;
        },
        keyup: function() {
            $number_value.text($(this).val());
        },
        change: function() {
            var $this = $(this);

            if ($this.val() != $this.data('value')) {
                $this.attr('name', $this.data('name'));
            } else {
                $this.removeAttr('name');
            }
        }
    });

    function show_info($input) {
        $coord_x_value.text($input.data('x'));
        $coord_y_value.text($input.data('y'));

        $number_value.text($input.val());
    }

    function hide_info() {
        $coord_x_value.text('');
        $coord_y_value.text('');

        $number_value.text('');
    }
});
