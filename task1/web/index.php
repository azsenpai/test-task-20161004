<?php

require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'config', 'bootstrap.php']));

$config = require(path_join(BASE_DIR, 'config', 'web.php'));
$app = Application::getInstance($config);

$app->run();
