<?php

return [
    'layout' => path_join(BASE_DIR, 'views', 'layouts', 'main.php'),

    'default_controller_name' => 'main',
    'default_action_name' => 'index',

    'db' => require(path_join(__DIR__, 'db.php')),

    'N' => 100,

    'MIN_NUMBER' => 1,
    'MAX_NUMBER' => 99999,
];
