<?php

function dump($var, $exit = true)
{
    printf('<pre>%s</pre>', print_r($var, true));

    if ($exit) {
        exit();
    }
}

function get_value(&$var, $default = null)
{
    return isset($var) ? $var : $default;
}

function safe_value($value)
{
    return htmlspecialchars($value);
}

function norm_path($path)
{
    return str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
}

function path_join()
{
    return implode(DIRECTORY_SEPARATOR, func_get_args());
}

function get_view_content($view, &$params)
{
    ob_start();

    extract($params);

    require(path_join(BASE_DIR, 'views', norm_path($view) . '.php'));

    $output = ob_get_contents();
    ob_end_clean();

    return $output;
}

function render($view, $params = [])
{
    global $app;

    ob_start();

    $content = get_view_content($view, $params);
    require($app->layout);

    $output = ob_get_contents();
    ob_end_clean();

    return $output;
}

function open_db($db_config = null)
{
    global $config;

    $dbh = new PDO($config['db']['dsn'], $config['db']['username'], $config['db']['password']);

    return $dbh;
}

function close_db($dbh)
{
    $dbh = null;
}

function execute_query($sql, $params = [])
{
    $dbh = open_db();

    $stmt = $dbh->prepare($sql);

    $result = $stmt->execute($params);

    close_db($dbh);

    return $result;
}

function query_all($sql, $params = [])
{
    $dbh = open_db();

    $stmt = $dbh->prepare($sql);

    $result = [];

    if ($stmt->execute()) {
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    close_db($dbh);

    return $result;
}
