<?php

define('BASE_DIR', dirname(__DIR__));

require_once('functions.php');

require_once(path_join(BASE_DIR, 'core', 'Application.php'));
require_once(path_join(BASE_DIR, 'core', 'controllers', 'Controller.php'));
