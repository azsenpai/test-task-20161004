<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= get_value($params['title'], 'task1') ?></title>
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/style.css">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/script.js"></script>
    </head>
    <body>
        <div class="wrapper">
            <?= $content ?>
        </div>
    </body>
</html>