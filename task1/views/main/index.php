<form class="form" action="/" method="post">
    <div class="info">
        <span class="coord coord-x">
            <b>x:</b> <span class="value"></span>
        </span>
        <span class="coord coord-y">
            <b>y:</b> <span class="value"></span>
        </span>
        <span class="number">
            <b>value:</b> <span class="value"></span>
        </span>
    </div>
    <div class="form-wrapper-outer">
        <div class="form-wrapper">
            <?php for ($i = 0; $i < $n; $i ++): ?>
                <div class="row">
                    <?php for ($j = 0; $j < $n; $j ++): ?>
                        <input class="form-input form-input-a"
                            value="<?= get_value($a[$i][$j]) ?>"
                            data-name="a[<?= $i ?>][<?= $j ?>]"
                            data-x="<?= $j + 1 ?>"
                            data-y="<?= $i + 1 ?>"
                            data-value="<?= get_value($a[$i][$j]) ?>">
                    <?php endfor ?>
                </div>
            <?php endfor ?>
        </div>
    </div>
    <div class="text-center">
        <input type="submit" value="Сохранить">
    </div>
</form>