<?php

class Application
{
    private static $_instance;

    public $layout;
    public $params;

    public $config;

    public $controller_name;
    public $action_name;

    public $default_controller_name;
    public $default_action_name;

    private function Application($config)
    {
        $this->init($config);
    }

    protected function __clone()
    {
    }

    public static function getInstance($config = [])
    {
        if (self::$_instance === null) {
            self::$_instance = new self($config);
        }

        return self::$_instance;
    }

    public function init($config)
    {
        $this->config = $config;

        $this->layout = get_value($config['layout']);

        $this->default_controller_name = get_value($config['default_controller_name']);
        $this->default_action_name = get_value($config['default_action_name']);
    }

    public function run()
    {
        $uri = $this->get_uri();
        $list = $uri ? explode('/', $uri) : [];

        switch (count($list)) {
            case 0:
                $this->controller_name = $this->default_controller_name;
                $this->action_name = $this->default_action_name;

                break;

            case 1:
                $this->controller_name = $this->default_controller_name;
                $this->action_name = $list[0];

                break;

            case 2:
                $this->controller_name = $list[0];
                $this->action_name = $list[1];

                break;
        }

        $this->import_controller();

        $controller_class = $this->norm_controller_name();
        $action = $this->norm_action_name();

        $controller = new $controller_class;

        $content = method_exists($controller, $action)
            ? $controller->$action()
            : $this->error();

        print($content);
    }

    public function error()
    {
        $this->controller_name = $this->default_controller_name;
        $this->action_name = 'error';

        $this->import_controller();

        $controller_class = $this->norm_controller_name();
        $action = $this->norm_action_name();

        $controller = new $controller_class;

        return $controller->$action();
    }

    public function import_controller()
    {
        $controller_path = path_join(BASE_DIR, 'controllers', $this->norm_controller_name() . '.php');

        if (!file_exists($controller_path)) {
            $this->controller_name = $this->default_controller_name;
            $this->action_name = 'error';
   
            $controller_path = path_join(BASE_DIR, 'controllers', $this->norm_controller_name() . '.php');
        }

        require_once($controller_path);
    }

    public function import_model($model_name)
    {
        $model_path = path_join(BASE_DIR, 'models', $model_name . '.php');

        if (file_exists($model_path)) {
            require_once($model_path);
        }
    }

    public function norm_controller_name()
    {
        $name = strtolower($this->controller_name);

        for ($i = 0; $i < strlen($name); $i ++) {
            if ($i == 0 || $name[$i-1] == '-') {
                $name[$i] = strtoupper($name[$i]);
            }
        }

        $name = str_replace('-', '', $name);

        return $name . 'Controller';
    }

    public function norm_action_name()
    {
        $name = strtolower($this->action_name);

        for ($i = 0; $i < strlen($name); $i ++) {
            if ($i == 0 || $name[$i-1] == '-') {
                $name[$i] = strtoupper($name[$i]);
            }
        }

        $name = str_replace('-', '', $name);

        return 'action' . $name;
    }

    public function is_get()
    {
        return strtoupper($_SERVER['REQUEST_METHOD']) === 'GET';
    }

    public function is_post()
    {
        return strtoupper($_SERVER['REQUEST_METHOD']) === 'POST';
    }

    public function get($key = null, $default = null)
    {
        if ($key === null) {
            return $_GET;
        }

        return get_value($_GET[$key], $default);
    }

    public function post($key = null, $default = null)
    {
        if ($key === null) {
            return $_POST;
        }

        return get_value($_POST[$key], $default);
    }

    public function redirect($url = '/', $permanent = false)
    {
        if (headers_sent() === false) {
            header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
        }

        exit();
    }

    function get_uri($with_params = false)
    {
        $uri = trim($_SERVER['REQUEST_URI'], '/');

        if (!$with_params) {
            $pos = strpos($uri, '?');
            $pos = ($pos === false) ? strlen($uri) : $pos;

            $uri = substr($uri, 0, $pos);
        }

        return $uri;
    }
}
