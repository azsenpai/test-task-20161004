<?php

class Controller
{
    public function render($view, $params = [])
    {
        if (strpos($view, '/') === false) {
            $class_name = get_class($this);
            $class_name = substr($class_name, 0, strlen($class_name) - strlen('Controller'));

            $controller = '';

            for ($i = 0; $i < strlen($class_name); $i ++) {
                if (ctype_upper($class_name[$i])) {
                    if ($i > 0) {
                        $controller .= '-';
                    }
                    $controller .= strtolower($class_name[$i]);
                } else {
                    $controller .= $class_name[$i];
                }
            }

            $view = $controller . '/' . $view;
        }

        return render($view, $params);
    }
}
