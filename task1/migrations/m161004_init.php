<?php

require(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'config', 'bootstrap.php']));

$config = require(path_join(BASE_DIR, 'config', 'web.php'));

function up()
{
    global $config;

    $sql = 'DROP TABLE `number`';
    execute_query($sql);

    $sql = '
        CREATE TABLE `number` (
        	`id` INT(11) NOT NULL,
        	`value` INT(11) NULL DEFAULT NULL,
        	PRIMARY KEY (`id`)
        )
        ENGINE=MyISAM
    ';
    execute_query($sql);

    $n = $config['N'];

    $min_number = $config['MIN_NUMBER'];
    $max_number = $config['MAX_NUMBER'];

    $used = [];
    $values = [];

    for ($i = 0; $i < $n; $i ++) {
        $x = rand(0, $n - 1);
        $y = rand(0, $n - 1);

        $id = $x * $n + $y;

        while (isset($used[$id])) {
            $x = rand(0, $n - 1);
            $y = rand(0, $n - 1);

            $id = $x * $n + $y;
        }

        $used[$id] = true;

        $value = rand($min_number, $max_number);

        $values[] = sprintf('(%d, %d)', $id, $value);
    }

    if (!empty($values)) {
        $sql = sprintf('INSERT INTO `number` VALUES %s', implode(', ', $values));
        execute_query($sql);
    }
}

function down()
{
    $sql = 'DROP TABLE `number`';
    execute_query($sql);
}


$type = get_value($argv[1], 'up');

switch ($type) {
    case 'up':
        up();
        break;

    case 'down':
        down();
        break;
}
