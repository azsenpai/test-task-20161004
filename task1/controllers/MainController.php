<?php

class MainController extends Controller
{
    public function actionIndex()
    {
        global $app;

        $n = $app->config['N'];

        if ($app->is_post()) {
            $a = $app->post('a', []);

            $values = [];

            foreach ($a as $x => $b) {
                foreach ($b as $y => $value) {
                    $id = $x * $n + $y;

                    if ($value === '') {
                        $value = 'NULL';
                    }

                    $values[] = sprintf('(%d, %s)', $id, $value);
                }
            }

            if (!empty($values)) {
                $sql = sprintf('REPLACE INTO `number` VALUES %s', implode(', ', $values));
                execute_query($sql);
            }
        }

        $a = [];

        $sql = 'SELECT * FROM number';
        $result = query_all($sql);

        foreach ($result as $item) {
            $x = intval($item['id'] / $n);
            $y = $item['id'] % $n;

            $a[$x][$y] = $item['value'];
        }

        return $this->render('index', [
            'n' => $n,
            'a' => $a,
        ]);
    }

    public function actionError()
    {
        return $this->render('error');
    }
}
