<?php

// O(N*log(N))

require('generate.php');

$sol_time_start = microtime(true);

// print_r($a);

sort($a);

for ($i = 1; $i < N; $i ++) {
    if ($a[$i] == $a[$i-1]) {
        print($a[$i]);
        break;
    }
}

$sol_time_end = microtime(true);

if (SHOW_TIME) {
    print(PHP_EOL);
    printf('generate time: %.5f seconds', $gen_time_end - $gen_time_start);

    print(PHP_EOL);
    printf('solution time: %.5f seconds', $sol_time_end - $sol_time_start);
}
