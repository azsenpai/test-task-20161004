<?php

define('SLOW_GENERATE', false);
define('SHOW_TIME', true);

define('N', 1000000);

define('MIN_NUMBER', 0);

if (PHP_INT_SIZE > 4) {
    define('MAX_NUMBER', (int)pow(2, 32));
} else {
    define('MAX_NUMBER', (int)(pow(2, 31) - 1));
}

$gen_time_start = microtime(true);

$a = [];

if (SLOW_GENERATE) {
    $used = [];

    for ($i = 1; $i < N; $i ++) {
        $x = rand(MIN_NUMBER, MAX_NUMBER);

        while (isset($used[$x])) {
            $x = rand(MIN_NUMBER, MAX_NUMBER);
        }

        $used[$x] = true;

        $a[] = $x;
    }
} else {
    $start = rand(MIN_NUMBER, MAX_NUMBER - N);
    $a = range($start, $start + N);
}

$i = rand(1, N - 1) - 1;
$a[] = $a[$i];

shuffle($a);

$gen_time_end = microtime(true);
