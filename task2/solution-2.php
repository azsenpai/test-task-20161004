<?php

// O(N) + O(N) memory

ini_set('memory_limit', '256M');

require('generate.php');

$sol_time_start = microtime(true);

// print_r($a);

$used = [];

for ($i = 0; $i < N; $i ++) {
    if (isset($used[$a[$i]])) {
        print($a[$i]);
    }

    $used[$a[$i]] = true;
}

$sol_time_end = microtime(true);

if (SHOW_TIME) {
    print(PHP_EOL);
    printf('generate time: %.5f seconds', $gen_time_end - $gen_time_start);

    print(PHP_EOL);
    printf('solution time: %.5f seconds', $sol_time_end - $sol_time_start);
}
