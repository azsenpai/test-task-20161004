<?php

namespace app\controllers;

use Yii;
use app\models\Author;
use app\models\Book;
use app\models\BookAuthor;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\helpers\ArrayHelper;
use app\helpers\VarDumper;

/**
 * AuthorController implements the CRUD actions for Author model.
 */
class AuthorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Author models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Author::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Author model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $bookIds = BookAuthor::find()
            ->select('book_id')
            ->where(['author_id' => $id])
            ->column();

        $dataProvider = new ActiveDataProvider([
            'query' => Book::find()->where(['id' => $bookIds]),
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Author model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Author();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $books = Yii::$app->request->post('books');

            if (!empty($books)) {
                $columns = ['book_id', 'author_id'];
                $rows = [];

                foreach ($books as $book_id) {
                    $rows[] = [$book_id, $model->id];
                }

                Yii::$app->db->createCommand()
                    ->batchInsert(BookAuthor::tableName(), $columns, $rows)
                    ->execute();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $books = Book::find()
                ->asArray()
                ->all();

            $books = ArrayHelper::map($books, 'id', 'name');

            return $this->render('create', [
                'model' => $model,
                'books' => $books,
            ]);
        }
    }

    /**
     * Updates an existing Author model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            BookAuthor::deleteAll(['author_id' => $model->id]);

            $books = Yii::$app->request->post('books');

            if (!empty($books)) {
                $columns = ['book_id', 'author_id'];
                $rows = [];

                foreach ($books as $book_id) {
                    $rows[] = [$book_id, $model->id];
                }

                Yii::$app->db->createCommand()
                    ->batchInsert(BookAuthor::tableName(), $columns, $rows)
                    ->execute();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $bookIds = BookAuthor::find()
                ->select('book_id')
                ->where(['author_id' => $id])
                ->column();

            $books = Book::find()
                ->asArray()
                ->all();

            $books = ArrayHelper::map($books, 'id', 'name');

            return $this->render('update', [
                'model' => $model,
                'bookIds' => $bookIds,
                'books' => $books,
            ]);
        }
    }

    /**
     * Deletes an existing Author model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Author model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Author the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Author::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
