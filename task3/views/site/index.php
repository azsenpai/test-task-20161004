<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Task3';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Task3</h1>

        <p><?= Html::a('Books', ['book/index'], ['class' => 'btn btn-lg btn-success']); ?></p>
        <p><?= Html::a('Authors', ['author/index'], ['class' => 'btn btn-lg btn-primary']); ?></p>
    </div>

    <div class="body-content">
    </div>

</div>
