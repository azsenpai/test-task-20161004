<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'published_at')->textInput(['class' => 'form-control datepicker']) ?>

    <div class="form-group field-book-authors">
        <label class="control-label" for="book-authors">Authors</label>
        <?= Html::dropDownList('authors', $authorIds, $authors, [
            'id' => 'book-authors',
            'multiple' => true,
            'class' => 'form-control select2',
        ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
