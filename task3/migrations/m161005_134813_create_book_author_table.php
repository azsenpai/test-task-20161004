<?php

use yii\db\Migration;

/**
 * Handles the creation for table `book_author`.
 * Has foreign keys to the tables:
 *
 * - `book`
 * - `author`
 */
class m161005_134813_create_book_author_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('book_author', [
            'id' => $this->primaryKey(),
            'book_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `book_id`
        $this->createIndex(
            'idx-book_author-book_id',
            'book_author',
            'book_id'
        );

        // add foreign key for table `book`
        $this->addForeignKey(
            'fk-book_author-book_id',
            'book_author',
            'book_id',
            'book',
            'id',
            'CASCADE'
        );

        // creates index for column `author_id`
        $this->createIndex(
            'idx-book_author-author_id',
            'book_author',
            'author_id'
        );

        // add foreign key for table `author`
        $this->addForeignKey(
            'fk-book_author-author_id',
            'book_author',
            'author_id',
            'author',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `book`
        $this->dropForeignKey(
            'fk-book_author-book_id',
            'book_author'
        );

        // drops index for column `book_id`
        $this->dropIndex(
            'idx-book_author-book_id',
            'book_author'
        );

        // drops foreign key for table `author`
        $this->dropForeignKey(
            'fk-book_author-author_id',
            'book_author'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-book_author-author_id',
            'book_author'
        );

        $this->dropTable('book_author');
    }
}
