<?php

namespace app\helpers;

class VarDumper extends \yii\helpers\VarDumper
{
    public static function dump($var, $depth = 10, $highlight = true, $exit = true)
    {
        parent::dump($var, $depth, $highlight);

        if ($exit) {
            exit();
        }
    }
}
