jQuery(function($) {
    $('.select2').select2();

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        orientation: 'bottom'
    });
});
