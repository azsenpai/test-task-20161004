<?php

namespace app\assets;

use yii\web\AssetBundle;

class DatepickerAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-datepicker/dist';

    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public function init()
    {
        if (YII_ENV_DEV) {
            $this->css[] = 'css/bootstrap-datepicker.css';
            $this->js[] = 'js/bootstrap-datepicker.js';
        } else {
            $this->css[] = 'css/bootstrap-datepicker.min.css';
            $this->js[] = 'js/bootstrap-datepicker.min.js';
        }

        parent::init();
    }
}
