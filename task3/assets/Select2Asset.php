<?php

namespace app\assets;

use yii\web\AssetBundle;

class Select2Asset extends AssetBundle
{
    public $sourcePath = '@bower/select2/dist';

    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public function init()
    {
        if (YII_ENV_DEV) {
            $this->css[] = 'css/select2.css';
            $this->js[] = 'js/select2.js';
        } else {
            $this->css[] = 'css/select2.min.css';
            $this->js[] = 'js/select2.min.js';
        }

        parent::init();
    }
}
